package luangudom.kantapong.lab9;

import javax.swing.SwingUtilities;


import luangudom.kantapong.lab8.MyFrameV4;

public class MyFrameV5 extends MyFrameV4 {

	public MyFrameV5(String text) {
		super(text);
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	public void addComponents() {
		add(new MyCanvasV5());
	}

}