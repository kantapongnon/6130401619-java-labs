package luangudom.kantapong.lab9;

/**
* This Java program inherits from MyCanvasV7, Draw 2D Graphic and send to MyFrameV8 (GUI version 8).
* This is the bouncing ball game. The game start when player press the space bar button and the ball 
* will bounce in randomly direction with 3 lives. Player need to hit the rest of bricks to defeat this game. 
* Note that when the ball pass the bottom lives will decrease and the ball will reborn on the pedal. 
* When lives reach 0, The game is over.
* 
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: April 10, 2019
* 
* */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import luangudom.kantapong.lab8.MyBall;
import luangudom.kantapong.lab8.MyBrick;
import luangudom.kantapong.lab8.MyFrame;
import luangudom.kantapong.lab8.MyPedal;

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {

	protected int numCol = WIDTH / MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks;
	protected Thread running;
	protected Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,
			Color.RED };
	protected MyPedalV2 pedal;
	protected int lives;

	public MyCanvasV8() {
		bricks = new MyBrickV2[numRow][numCol];
		ball = new MyBallV2(WIDTH / 2 - MyBall.diameter / 2, HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
		ball.ballVelX = 0;
		ball.ballVelY = 0;

		running = new Thread(this);

		// Append bricks into array.
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, super.HEIGHT / 3 - i * MyBrick.brickHeight);

			}
		}

		pedal = new MyPedalV2(WIDTH / 2 - MyPedal.pedalWidth / 2, MyCanvas.HEIGHT - MyPedal.pedalHeight);
		lives = 3;

		setFocusable(true);
		addKeyListener(this);
		running.start();

		/**
		 * @author Kantapong
		 */

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// Draw the filled rectangle the same size as the window.
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		repaint();

		// Use for loop to draw the rest of bricks.
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(5));
					g2d.setColor(Color.BLACK);
					g2d.draw(bricks[i][j]);

					/**
					 * @author Kantapong
					 */
				}
			}

		}

		// Draw the ball.
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

		// Draw the pedal.
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);

		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.BLUE);
		String s = " Lives : " + lives;
		g2d.drawString(s, 10, 30);

		// Display "You won" when number of bricks equal to 0.
		if (numVisibleBricks == 0) {
			String won = "You won";
			g2d.setFont(new Font("SanSerif", Font.BOLD, 50));
			g2d.setColor(Color.GREEN);
			g2d.drawString(won, super.WIDTH / 2 - 150, super.HEIGHT / 2);
		}

		// Display "GAME OVER" when lives equals to 0.
		if (lives == 0) {
			String over = "GAME OVER";
			g2d.setFont(new Font("SanSerif", Font.BOLD, 50));
			g2d.setColor(Color.DARK_GRAY);
			g2d.drawString(over, super.WIDTH / 2 - 150, super.HEIGHT / 2);
		}

	}

	// This method is use to check if the ball pass the bottom. If so, decrement
	// 1 live.
	private void checkPassBottom() {
		if (ball.y >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + MyPedal.pedalWidth / 2 - MyBall.diameter / 2;
			ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;

			lives--;
			repaint();

			/**
			 * @author Kantapong
			 */
		}
	}

	// This method is use to check if the ball collides with the brick.
	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {

			if (deltaX * deltaX < deltaY * deltaY) {

				ball.ballVelY *= -1;

			} else {

				ball.ballVelX *= -1;
			}
			// Move the ball
			ball.move();

			// The brick will disappear when hit by the ball.
			brick.visible = false;

			// Decrement number of the brick when hit by the ball.
			numVisibleBricks--;

			/**
			 * @author Kantapong
			 */
		}
	}

	// This method is use to check if the ball collides with the pedal.
	private void collideWithPedal() {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedalV2.pedalWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyPedalV2.pedalHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {

			if (deltaX * deltaX < deltaY * deltaY) {

				ball.ballVelY *= -1;

			} else {

				ball.ballVelX *= -1;

			}

			ball.move();

			/**
			 * @author Kantapong
			 */
		}

	}

	// This method is use to control pedal; move to the left when press the left
	// arrow key, move to the right when press right arrow key and random
	// the direction of the ball when start.
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			pedal.moveLeft();

		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			pedal.moveRight();

		} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {

			ball.ballVelY = 4;
			int max = 1;
			int min = 0;
			int random = (int) (Math.random() * ((max - min) + 1)) + min;
			if (random == 0) {
				ball.ballVelX = -4;
			} else {
				ball.ballVelX = 4;
			}

			/**
			 * @author Kantapong
			 */
		}

	}

	@Override
	public void run() {
		while (true) {

			// The ball will bouncing off if it collides with the left or right wall.
			if (ball.x >= super.WIDTH - MyBall.diameter || ball.x <= 0) {
				ball.ballVelX *= -1;

			}

			// Check if the ball pass the bottom.
			if (ball.y >= super.HEIGHT - MyBall.diameter) {

				checkPassBottom();

				// Check if live equals to 0.
				if (lives == 0) {
					break;
				}

				// The ball will bounce if it collides with the top.
			} else if (ball.y <= 0) {
				ball.ballVelY *= -1;
			}

			// Check if the ball collides with bricks.
			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkCollision(ball, bricks[i][j]);
					}
				}
			}

			// Check if ball collides with pedal.
			if (ball.y + ball.diameter >= pedal.y - pedal.height) {
				collideWithPedal();
			}

			ball.move();

			repaint();

			try {
				Thread.sleep(12);
			} catch (InterruptedException ex) {
				System.out.println("Error");
			}

			/**
			 * @author Kantapong
			 */
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
