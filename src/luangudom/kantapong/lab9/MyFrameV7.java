package luangudom.kantapong.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV7 extends MyFrameV6 {

	public MyFrameV7(String text) {
		super(text);
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV7 msw = new MyFrameV7("My Frame V7");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	public void addComponents() {
		add(new MyCanvasV7());
	}

}



