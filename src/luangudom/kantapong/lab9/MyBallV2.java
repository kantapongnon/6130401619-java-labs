package luangudom.kantapong.lab9;

import luangudom.kantapong.lab8.MyBall;

public class MyBallV2 extends MyBall {
	protected int ballVelX;
	protected int ballVelY;

	// Call super class�s constructor.
	public MyBallV2(int x, int y) {
		super(x, y);

	}

	// This method is use to increment coordinate x and y.
	public void move() {
		x += ballVelX;
		y += ballVelY;

		/**
		 * @author Kantapong
		 */
	}

}
