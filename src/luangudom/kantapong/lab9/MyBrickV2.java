package luangudom.kantapong.lab9;

import luangudom.kantapong.lab8.MyBrick;

public class MyBrickV2 extends MyBrick {
	protected boolean visible;

	// Call super class�s constructor and initialize visible to true.
	public MyBrickV2(int x, int y) {
		super(x, y);
		visible = true;

	}

}
