package luangudom.kantapong.lab9;

/**
* This Java program inherits from MyCanvasV6, Draw 2D Graphic and send to MyFrameV7 (GUI version 7)
* to display the moving ball from the upper left corner of the window to any direction, and bounce when hit 
* any sides of the window or hit the brick. The brick will disappear after collides.
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: April 10, 2019
* 
* */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import luangudom.kantapong.lab8.MyBall;
import luangudom.kantapong.lab8.MyBrick;

public class MyCanvasV7 extends MyCanvasV6 implements Runnable {
	protected int numBricks;
	protected MyBrickV2[] bricks;
	protected Thread running;

	public MyCanvasV7() {
		// Calculate the number of bricks.
		numBricks = MyCanvas.WIDTH / MyBrick.brickWidth;

		// Initialize variable ball with coordinate (0,0).
		ball = new MyBallV2(0, 0);

		// Initialize running.
		running = new Thread(this);

		// SetballVelX and ballVelY to 2.
		ball.ballVelX = 2;
		ball.ballVelY = 2;

		// Initialize the array brick.
		bricks = new MyBrickV2[numBricks];

		// Set the coordinate of bricks using loop.
		for (int i = 0; i < numBricks; i++) {
			bricks[i] = new MyBrickV2((MyBrick.brickWidth + 2) * i, MyCanvasV7.HEIGHT / 2);
		}

		running.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// Draw the filled rectangle the same size as the window.
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));

		// Draw the 2D ball.
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		g2d.draw(ball);
		repaint();

		// Use for loop to draw the rest of bricks.
		for (int i = 0; i < numBricks; i++) {
			g2d.setColor(Color.RED);
			g2d.fill(bricks[i]);
			g2d.setStroke(new BasicStroke(5));
			g2d.setColor(Color.BLUE);
			g2d.draw(bricks[i]);

			// To check if bricks have been hit. If so, the brick will disappear.
			if (bricks[i].visible != true) {
				g2d.setColor(Color.WHITE);
				g2d.fill(ball);
				repaint();
				g2d.setColor(Color.BLACK);
				g2d.fill(bricks[i]);
				g2d.draw(bricks[i]);

				/**
				 * @author Kantapong
				 */
			}

		}

	}

	@Override
	public void run() {
		while (true) {
			// To check if the ball hit any sides of the window. When hit set both x and y
			// to the invert directions.
			if (ball.x <= 0 || ball.x + MyBall.diameter >= MyCanvas.WIDTH) {
				ball.ballVelX *= -1;
			}

			if (ball.y <= 0 || ball.y + MyBall.diameter >= MyCanvas.HEIGHT) {
				ball.ballVelY *= -1;

			}

			// To check if bricks are visibly. If so, the brick is able to collides by the
			// ball.
			for (int i = 0; i < numBricks; i++) {
				if (bricks[i].visible) {
					checkCollision(ball, bricks[i]);

				}
			}

			ball.move();
			repaint();

			try {

				Thread.sleep(10);
			} catch (InterruptedException ex) {

				/**
				 * @author Kantapong
				 */

			}
		}

	}

	// This method is use to check when the ball collides the brick.
	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		// If the ball hit the brick, set both y and x into invert directions.
		if (collided) {

			if (deltaX * deltaX < deltaY * deltaY) {

				ball.ballVelY *= -1;

			} else {

				ball.ballVelX *= -1;

			}

			// Move the ball.
			ball.move();
			// The brick will no longer able to collides.
			brick.visible = false;

			/**
			 * @author Kantapong
			 */
		}
	}

}
