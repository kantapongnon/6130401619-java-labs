package luangudom.kantapong.lab9;

/**
* This Java program inherits from MyCanvasV5, Draw 2D Graphic and send to MyFrameV6 (GUI version 6)
* to display the moving ball from the center of the window to any direction, and stop when hit 
* any sides of the window.
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: April 10, 2019
* 
* */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import luangudom.kantapong.lab8.MyBall;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
	MyBallV2 ball;
	Thread running2;

	public MyCanvasV6() {
		// Initialize the position of the ball to the center of the window.
		ball = new MyBallV2(MyCanvas.WIDTH / 2, MyCanvas.HEIGHT / 2);
		running2 = new Thread(this);
		running2.start();
		ball.ballVelX = 1;
		ball.ballVelY = 1;
	}

	@Override
	public void run() {
		while (true) {

			if (ball.x <= 0 || ball.x + MyBall.diameter >= MyCanvas.WIDTH) {
				ball.ballVelX *= -1;
			}

			if (ball.y <= 0 || ball.y + MyBall.diameter >= MyCanvas.HEIGHT) {
				ball.ballVelY *= -1;

			}

			ball.move();
			repaint();

			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

				/**
				 * @author Kantapong
				 */
			}
		}

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// Draw the filled rectangle the same size as the window.
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		// Draw the 2D ball.
		g2d.setColor(Color.WHITE);
		g2d.draw(ball);
		g2d.fill(ball);

		/**
		 * @author Kantapong
		 */
	}

}
