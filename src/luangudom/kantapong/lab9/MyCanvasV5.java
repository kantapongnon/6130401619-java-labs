package luangudom.kantapong.lab9;

/**
* This Java program inherits from MyCanvasV4, Draw 2D Graphic and send to MyFrameV5 (GUI version 5)
* to display the moving ball from the left and stop when hit the right side of the window.
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: April 10, 2019
* 
* */
import java.awt.*;
import java.awt.geom.*;

import luangudom.kantapong.lab8.MyBall;
import luangudom.kantapong.lab8.MyCanvas;
import luangudom.kantapong.lab8.MyCanvasV4;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable {

	protected MyCanvas MyCanvas = new luangudom.kantapong.lab8.MyCanvas();
	protected MyBallV2 ball = new MyBallV2(0, MyCanvas.HEIGHT / 2 - MyBall.diameter / 2);
	protected Thread running = new Thread(this);

	public MyCanvasV5() {
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();

	}

	@Override
	public void run() {
		// To check whether the ball hit the right side of the window. If so, the ball
		// will
		// stop.
		while (true) {
			if (ball.x + MyBall.diameter >= MyCanvas.WIDTH) {
				break;
			}

			ball.move();
			repaint();

			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

				/**
				 * @author Kantapong
				 **/

			}

		}

	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// Draw the filled rectangle the same size as the window.
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.getWidth(), super.getHeight()));
		// Draw the 2D ball.
		g2d.setColor(Color.WHITE);
		g2d.draw(ball);
		g2d.fill(ball);

		/**
		 * @author Kantapong
		 */
	}

}