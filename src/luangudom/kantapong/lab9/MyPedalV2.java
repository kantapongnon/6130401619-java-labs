package luangudom.kantapong.lab9;

import luangudom.kantapong.lab8.MyCanvas;
import luangudom.kantapong.lab8.MyPedal;

public class MyPedalV2 extends MyPedal {
	protected final static int speedPedal = 20;

	// Call super class�s constructor.
	public MyPedalV2(int x, int y) {
		super(x, y);

	}

	// This method is use to control the pedal to move left.
	public void moveLeft() {
		// Check if the pedal arrive the left side of the window. If so, set
		// the position x to zero. If not, continue to move the pedal to the left.
		if (x - speedPedal < 0) {
			x = 0;
		} else {
			x -= 50;
		}

		/**
		 * @author Kantapong
		 */
	}

	// This method is use to control the pedal to move right.
	public void moveRight() {
		// Check if the pedal arrive the right side of the window. If so, set
		// the position x to MyCanvas.WIDTH - MyPedal.pedalWidth. If not, continue to
		// move the pedal to the right.
		if (speedPedal + x > MyCanvas.WIDTH - MyPedal.pedalWidth) {
			x = MyCanvas.WIDTH - MyPedal.pedalWidth;

		} else {
			x += MyPedal.pedalWidth / 2;

		}

		/**
		 * @author Kantapong
		 */
	}

}
