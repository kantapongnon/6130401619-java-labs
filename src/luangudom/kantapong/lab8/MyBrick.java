package luangudom.kantapong.lab8;

/**
* This Java program is use to creates brick (rectangle shape)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double {
	public final static int brickWidth = 80;
	public final static int brickHeight = 20;

	public MyBrick(int x, int y) {
		// Call superclass passing the position (x,y), brickWidth and brickHeight
		// parameters.
		super(x, y, brickWidth, brickHeight);

	}
}
