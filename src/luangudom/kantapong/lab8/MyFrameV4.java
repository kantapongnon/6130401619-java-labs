package luangudom.kantapong.lab8;

/**
* This Java program inherits from MyFrameV3, Create and show GUI (version 4)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import javax.swing.SwingUtilities;

public class MyFrameV4 extends MyFrameV3 {

	public MyFrameV4(String text) {
		super(text);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV4("My Frame V4");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	public void addComponents() {
		add(new MyCanvasV4());
	}

}
