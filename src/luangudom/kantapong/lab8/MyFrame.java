package luangudom.kantapong.lab8;

/**
* This Java program inherits from JFrame, Create and show GUI (version 1)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame {
	public MyFrame(String msw) {
		setTitle(msw);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	protected void addComponents() {
		add(new MyCanvas());
	}

	public void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
