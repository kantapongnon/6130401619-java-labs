package luangudom.kantapong.lab8;

/**
* This Java program inherits from MyCanvas, Draw 2D graphics and send to MyFrameV2 (GUI version 2)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV2 extends MyCanvas {
	protected MyBall ball;
	protected MyPedal pedal;
	protected MyBrick brick;

	public void paintComponent(Graphics g) {
		// Call superclass's preset.
		super.paintComponent(g);

		// Set the position of all components.
		ball = new MyBall(WIDTH / 2 - 15, HEIGHT / 2 - 15);
		pedal = new MyPedal(WIDTH / 2 - 50, HEIGHT - 12);
		brick = new MyBrick(WIDTH / 2 - 40, 0);

		Graphics2D g_2D = (Graphics2D) g;

		g_2D.setColor(Color.BLACK);
		g_2D.fillRect(0, 0, WIDTH, HEIGHT);

		g_2D.setColor(Color.WHITE);

		// Draw all of the components
		g_2D.fill(ball);
		g_2D.draw(ball);

		g_2D.fill(pedal);
		g_2D.draw(pedal);

		g_2D.fill(brick);
		g_2D.draw(brick);

		g_2D.drawLine(WIDTH / 2, 0, WIDTH / 2, HEIGHT);

		g_2D.drawLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2);

	}
}
