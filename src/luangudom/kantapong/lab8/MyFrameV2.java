package luangudom.kantapong.lab8;

/**
* This Java program inherits from MyFrame, Create and show GUI (version 2)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import javax.swing.SwingUtilities;

public class MyFrameV2 extends MyFrame {

	public MyFrameV2(String text) {
		super(text);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("MyFrame V2");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void addComponents() {
		add(new MyCanvasV2());
	}

}
