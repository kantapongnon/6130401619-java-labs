package luangudom.kantapong.lab8;

/**
* This Java program inherits from MyCanvasV2, Draw 2D graphics and send to MyFrameV3 (GUI version 3)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import java.awt.*;

public class MyCanvasV3 extends MyCanvasV2 {

	public void paintComponent(Graphics g) {
		// Call superclass's preset.
		super.paintComponent(g);
		Graphics2D g_2D = (Graphics2D) g;

		// Draw a rectangle the size of the whole panel.
		g_2D.setColor(Color.BLACK);
		g_2D.fillRect(0, 0, WIDTH, HEIGHT);

		MyBall[] fourBalls = new MyBall[4];
		MyBrick[] tenBricks = new MyBrick[10];

		for (int i = 0; i < fourBalls.length; i++) {

			// Use if else to set the position of the balls to 4 corners of window.
			if (i == 0) {
				ball.x = 0;
				ball.y = 0;
			} else if (i == 1) {
				ball.x = WIDTH - ball.diameter - 2;

			} else if (i == 2) {
				ball.x = 0;
				ball.y = HEIGHT - ball.diameter - 2;

			} else {
				ball.x = WIDTH - ball.diameter - 2;
				ball.y = HEIGHT - ball.diameter - 2;
			}

			g_2D.setColor(Color.WHITE);
			g_2D.fill(ball);

			// Add balls into array and draw it.
			fourBalls[i] = ball;
			g_2D.draw(fourBalls[i]);
		}

		brick.y = HEIGHT / 2;
		for (int j = 0; j < tenBricks.length; j++) {
			g_2D.setColor(Color.WHITE);

			// Set the position of the bricks, add them into array and draw it.
			brick.x = 2 + (j * (brick.brickWidth + 2));
			tenBricks[j] = brick;
			g_2D.fill(tenBricks[j]);
			g_2D.setColor(Color.BLACK);
			g_2D.draw(tenBricks[j]);
		}

	}

}
