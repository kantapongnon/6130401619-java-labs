package luangudom.kantapong.lab8;

/**
* This Java program is use to creates ball (Circle shape).
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import java.awt.geom.Ellipse2D;

public class MyBall extends Ellipse2D.Double {
	public final static int diameter = 30;

	public MyBall(int x, int y) {
		// Call superclass passing the position (x,y) and diameter parameters.
		super(x, y, diameter, diameter);
	}
}
