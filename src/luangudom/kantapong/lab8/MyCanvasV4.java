package luangudom.kantapong.lab8;

/**
* This Java program inherits from MyCanvasV3, Draw 2D graphics and send to MyFrameV4 (GUI version 4)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {

	public void paintComponent(Graphics g) {
		// Call superclass's preset.
		super.paintComponent(g);
		Graphics2D g_2D = (Graphics2D) g;

		// Draw a rectangle the size of the whole panel.
		g_2D.setColor(Color.BLACK);
		g_2D.fillRect(0, 0, WIDTH, HEIGHT);

		// Create a two dimensional array called bricks with 7 rows and 10 columns.
		MyBrick[][] bricks = new MyBrick[10][7];

		// Create a seven colors array.
		Color[] color = { Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA };

		// Use for loop to set the color, start position for each row of the bricks and
		// add them into bricks array.
		for (int i = 0; i < 10; i++) {

			for (int j = 0; j < 7; j++) {

				// Set the start position of the bricks
				brick.x = 0 + (i * (brick.brickWidth + 2));
				brick.y = ((HEIGHT / 3) - (20 * 7)) + (j * (brick.brickHeight + 2));

				// Set color for each row of the bricks.
				g_2D.setColor(color[j]);
				g_2D.fill(brick);

				// Add bricks into array and draw it.
				bricks[i][j] = brick;
				g_2D.setColor(Color.BLACK);
				g_2D.draw(bricks[i][j]);

			}

			// Set the position and color of the pedal then draw it.
			g_2D.setColor(Color.GRAY);
			g_2D.fill(pedal);
			pedal.x = (WIDTH / 2) - 50;
			pedal.y = HEIGHT - 12;
			g_2D.draw(pedal);

			// Set the position and color of the ball then draw it.
			g_2D.setColor(Color.WHITE);
			ball.x = (WIDTH / 2) - 15;
			ball.y = HEIGHT - 42;
			g_2D.draw(ball);
			g_2D.fill(ball);

		}

	}
}
