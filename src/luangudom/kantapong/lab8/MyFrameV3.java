package luangudom.kantapong.lab8;

/**
* This Java program inherits from MyFrameV2, Create and show GUI (version 3)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2 {

	public MyFrameV3(String text) {
		super(text);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	public void addComponents() {
		add(new MyCanvasV3());
	}

}
