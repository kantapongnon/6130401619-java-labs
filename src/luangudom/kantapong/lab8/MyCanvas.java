package luangudom.kantapong.lab8;

/**
* This Java program inherits from JPanel, Draw 2D graphics and send to MyFrame (GUI version 1)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;
	protected JPanel largePanel;
	protected Ellipse2D.Double face;
	protected Ellipse2D.Double leftEye;
	protected Ellipse2D.Double rightEye;
	protected Rectangle2D.Double mouth;

	public MyCanvas() {
		// Set the presets of ancestor class's method.
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setBackground(Color.BLACK);
	}

	public void paintComponent(Graphics g) {
		// Call superclass's preset.
		super.paintComponent(g);
		Graphics2D g_2D = (Graphics2D) g;

		// Set the position of all components and draw them.
		face = new Ellipse2D.Double(WIDTH - 550, HEIGHT - 450, 300, 300);
		leftEye = new Ellipse2D.Double(345, 250, 30, 60);
		rightEye = new Ellipse2D.Double(420, 250, 30, 60);
		mouth = new Rectangle2D.Double(350, 375, 100, 10);

		g_2D.setColor(Color.WHITE);
		g_2D.draw(face);

		g_2D.draw(leftEye);
		g_2D.fill(leftEye);
		g_2D.draw(rightEye);
		g_2D.fill(rightEye);
		g_2D.draw(mouth);
		g_2D.fill(mouth);

	}

}
