package luangudom.kantapong.lab8;

/**
* This Java program is use to creates pedal (rectangle shape)
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: March 30, 2019
* 
* */

import java.awt.geom.Rectangle2D;

public class MyPedal extends Rectangle2D.Double {
	public final static int pedalWidth = 100;
	public final static int pedalHeight = 10;

	public MyPedal(int x, int y) {
		// Call superclass passing the position (x,y), pedalWidth and pedalHeight
		// parameters.
		super(x, y, pedalWidth, pedalHeight);

	}
}
