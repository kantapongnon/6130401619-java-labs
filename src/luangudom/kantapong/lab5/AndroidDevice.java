package luangudom.kantapong.lab5;

/**
 * This is an abstract class that has a method usage() and provided operation
 * system for the Android device.
 * 
 * @author Kantapong Luangudom
 * @version 1.0
 * @since 2019-02-18
 * 
 */

public abstract class AndroidDevice {
	private static final String os = "Android";

	/**
	 * Declare this method without body.
	 */
	public void usage() {

	}

	/**
	 * This method is use to provided device's operation system.
	 * 
	 * @return device's operation system
	 */
	public String getOS() {
		return os;
	}

}
