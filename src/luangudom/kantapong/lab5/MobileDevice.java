package luangudom.kantapong.lab5;

import java.io.Serializable;

/**
 * This MobileDevice program is use to determined a mobile device specifications
 * include modelName, os, price and weight.
 * 
 * @author Kantapong Luangudom
 * @version 1.0
 * @since 2019-02-18
 * 
 */

public class MobileDevice implements java.lang.Comparable<MobileDevice>, Serializable{
	private String modelName;
	private String os;
	private int price;
	private int weight;

	/**
	 * This is a constructor of program to accepts values for the attributes
	 * modelName, os, price and weight.
	 * 
	 * @param modelName provided device's name.
	 * @param os        provided device's operation system.
	 * @param price     provided price of this device.
	 * @param weight    provided device's weight.
	 */
	public MobileDevice(String modelName, String os, int price, int weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}

	/**
	 * This is the same constructor but excluded device's weight, so this
	 * constructor set weight to zero.
	 * 
	 * @param modelName provided device's name.
	 * @param os        provided device's operation system.
	 * @param price     provided price of this device.
	 * 
	 */
	public MobileDevice(String modelName, String os, int price) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = 0;
	}

	/**
	 * This is the default constructor of program.
	 *
	 */
	public MobileDevice() {

	}

	/**
	 * This method is use to return device's name.
	 * 
	 * @return the name of the device
	 */
	public String getmodelName() {
		return modelName;
	}

	public void setmodelName(String modelName) {
		this.modelName = modelName;

	}

	/**
	 * This method is use to return device's operation system.
	 * 
	 * @return device's operation system
	 */
	public String getOS() {
		return os;
	}

	/**
	 * This method is use to set device's operation system to Android.
	 * 
	 * @return device's operation system (Android)
	 */
	public String setOsAD() {
		this.os = "Android";
		return os;
	}

	/**
	 * This method is use to return device's price.
	 * 
	 * @return the price of the device
	 */
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * This method is use to return device's weight.
	 * 
	 * @return device's weight
	 */
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * This method is use to set device's weight to zero.
	 * 
	 * @return zero weight
	 */
	public int setZeroWeight() {
		this.weight = 0;
		return weight;
	}

	public String toString() {
		return "MobileDevice [Model name: " + modelName + ", " + "OS: " + getOS() + ", " + "Price: " + price + " Baht,"
				+ " Weight: " + weight + " g]";

	}

	@Override
	public int compareTo(MobileDevice arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}
