package luangudom.kantapong.lab5;

/**
 * This SamsungDevice program is use to determined a Samsung mobile device
 * specifications include modelName, price, weight and androidVersion.
 * 
 * @author Kantapong Luangudom
 * @version 1.0
 * @since 2019-02-18
 * 
 */

//This class is a subclass of MobileDevice.
public class SamsungDevice extends MobileDevice {
	private static String brand = "Samsung";
	private double androidVersion;

	/**
	 * This is a constructor of program to accepts values for the attributes
	 * modelName, price and androidVersion.
	 * 
	 * @param modelName      provided device's name.
	 * @param price          provided price of this device.
	 * @param androidVersion provided devices version of android.
	 */
	// Use setOsAD() to set the os value to "Android" and set weight to zero.
	public SamsungDevice(String modelName, int price, double androidVersion) {
		super.setmodelName(modelName);
		super.setPrice(price);
		super.setOsAD();
		super.setZeroWeight();
		this.androidVersion = androidVersion;

	}

	/**
	 * This is the same constructor but included device's weight.
	 * 
	 * @param modelName      provided device's name.
	 * @param price          provided price of this device.
	 * @param weight         provide device's weight.
	 * @param androidVersion provided devices version of android.
	 */

	public SamsungDevice(String modelName, int price, int weight, double androidVersion) {
		super.setmodelName(modelName);
		super.setPrice(price);
		super.setOsAD();
		super.setWeight(weight);
		this.androidVersion = androidVersion;
	}

	/**
	 * This is the default constructor of program.
	 *
	 */
	public SamsungDevice() {

	}

	/**
	 * This method is use to provided device's brand.
	 * 
	 * @return device's brand name
	 */
	public static String getBrand() {
		return brand;
	}

	/**
	 * This method is use to display the information of device.
	 * 
	 * @return String message
	 */
	public String ToString() {
		return "SamsungDevice [Model name: " + super.getmodelName() + ", " + "OS: " + super.getOS() + ", " + "Price: "
				+ super.getPrice() + " Baht," + " Weight: " + super.getWeight() + " g]";

	}

	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch");
	}

}
