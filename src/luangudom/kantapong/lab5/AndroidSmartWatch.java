package luangudom.kantapong.lab5;

/**
 * This AndroidSmartWatch program is use to determined a Samsung watch device
 * specifications include brandName, modelName and price.
 * 
 * @author Kantapong Luangudom
 * @version 1.0
 * @since 2019-02-18
 * 
 */
//This class is a subclass of AndroidDevice.
public class AndroidSmartWatch extends AndroidDevice {
	private String modelName;
	private String brandName;
	private int price;

	/**
	 * This is a constructor of program to accepts values for the attributes
	 * brandName, modelName and price.
	 * 
	 * @param brandName provided device's brand.
	 * @param modelName provided device's name.
	 * @param price     provided price of this device.
	 * 
	 */
	public AndroidSmartWatch(String brandName, String modelName, int price) {
		this.brandName = brandName;
		this.modelName = modelName;
		this.price = price;
	}

	/**
	 * This is the default constructor of program.
	 *
	 */
	public AndroidSmartWatch() {

	}

	/**
	 * This method is use to return device's name.
	 * 
	 * @return the name of the device
	 */
	public String getmodelName() {
		return modelName;
	}

	public void setmodelName() {
		this.modelName = modelName;
	}

	/**
	 * This method is use to return device's brand name.
	 * 
	 * @return the name of the device's brand
	 */
	public String getbrandName() {
		return brandName;
	}

	public void setbrandName() {
		this.brandName = brandName;
	}

	/**
	 * This method is use to return device's price.
	 * 
	 * @return the prices of the device
	 */
	public int getprice() {
		return price;
	}

	public void setprice() {
		this.price = price;
	}

	// call a method usage() to send the usage output of device.
	public void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate and your step count");
	}

	public String toString() {
		return "AndroidSmartWatch [Brand name: " + brandName + ", " + "Model name: " + modelName + ", " + "Price: "
				+ price + " Baht]";

	}

	public void displayTime() {
		System.out.println("Display time only using a digital format");
	}

}
