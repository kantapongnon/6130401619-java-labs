package luangudom.kantapong.lab7;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
	private JPanel reviewandTextPanel;
	private JPanel imageandButtonPanel;
	
	

	public MobileDeviceFormV6(String title) {
		super(title);

	}

	protected void addComponents() {
		// Call super class to add components as the following below.
		super.addComponents();
		addImage phone = new addImage();
		reviewandTextPanel = new JPanel();
		imageandButtonPanel = new JPanel();
		
		
		imageandButtonPanel.setLayout(new BorderLayout());
		reviewandTextPanel.setLayout(new BorderLayout());
		
		reviewandTextPanel.add(reviewLabel, BorderLayout.NORTH);
		reviewandTextPanel.add(textScroll, BorderLayout.SOUTH);
		
		imageandButtonPanel.add(phone, BorderLayout.NORTH);
		imageandButtonPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		textandButtonPanel.add(reviewandTextPanel, BorderLayout.NORTH);
		textandButtonPanel.add(imageandButtonPanel, BorderLayout.SOUTH);
		
		
		
		
		allPanel.add(textandButtonPanel, BorderLayout.SOUTH);

		//newImagePanel.setLayout(new BorderLayout());
		
		

	}

	public static void createAndShowGUI() {

		// Set the name of the window.
		MobileDeviceFormV6 MobileDeviceFormV6 = new MobileDeviceFormV6("Mobile Device Form V6");

		// Call methods to add components and set features of window.
		MobileDeviceFormV6.addComponents();
		MobileDeviceFormV6.addMenus();
		MobileDeviceFormV6.setFrameFeatures();

	}

	protected void addMenus() {
		super.addMenus();

	}

	protected void updateMenuIcon() {
		super.updateMenuIcon();
	}

	protected void addSubMenus() {
		super.addSubMenus();

	}

	class addImage extends JPanel {
		protected BufferedImage img;
		protected String file = "images/galaxyNote9.jpg";

		public addImage() {
			try {
				img = ImageIO.read(new File(file));
			} catch (IOException e) {
				e.printStackTrace(System.err);
			}
		}

		public Dimension getPreferredSize() {
			if (img == null)
				return new Dimension(100, 100);
			else
				return new Dimension(img.getWidth() - 70 , img.getHeight() + 10);
		}

		protected void paintComponent(Graphics g) {

			g.drawImage(img, -5, 0, null);
		}

	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}

}
