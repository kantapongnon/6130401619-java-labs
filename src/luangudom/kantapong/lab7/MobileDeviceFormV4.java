package luangudom.kantapong.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import luangudom.kantapong.lab6.MobileDeviceFormV2;
import luangudom.kantapong.lab6.MobileDeviceFormV3;

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	protected ImageIcon newIcon;
	protected JMenuItem sixteenSize;
	protected JMenuItem twentySize;
	protected JMenuItem twentyFourSize;
	protected JMenuItem redSubmenu;
	protected JMenuItem greenSubmenu;
	protected JMenuItem blueSubmenu;

	protected JMenu color;
	protected JMenu size;

	public MobileDeviceFormV4(String title) {
		super(title);

	}

	public static void createAndShowGUI() {

		// Set the name of the window.
		MobileDeviceFormV4 MobileDeviceFormV4 = new MobileDeviceFormV4("Mobile Device Form V4");

		// Call methods to add components and set features of window.
		MobileDeviceFormV4.addComponents();
		MobileDeviceFormV4.addMenus();
		MobileDeviceFormV4.setFrameFeatures();

	}

	protected void addComponents() {
		// Call super class to add components as the following below.
		super.addComponents();
	}

	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenus();
	}

	protected void updateMenuIcon() {
		newIcon = new ImageIcon("images/new.jpg");
		newItem.setIcon(newIcon);
	}

	protected void addSubMenus() {
		
		configMenu.remove(colorItem);
		configMenu.remove(sizeItem);

		sixteenSize = new JMenuItem("16");
		twentySize = new JMenuItem("20");
		twentyFourSize = new JMenuItem("24");
		redSubmenu = new JMenuItem("Red");
		greenSubmenu = new JMenuItem("Green");
		blueSubmenu = new JMenuItem("Blue");

		color = new JMenu("Color");
		size = new JMenu("Size");

		color.add(redSubmenu);
		color.add(greenSubmenu);
		color.add(blueSubmenu);

		size.add(sixteenSize);
		size.add(twentySize);
		size.add(twentyFourSize);

		configMenu.add(color);
		configMenu.add(size);

	}
	
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}

}
