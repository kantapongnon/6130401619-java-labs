package luangudom.kantapong.lab7;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {
	protected Font plain14Font;
	protected Font bold14Font;
	
	public MobileDeviceFormV5(String title) {
		super(title);
		
	}
	
	protected void addComponents() {
		// Call super class to add components as the following below.
		super.addComponents();
		
		plain14Font = new Font("Serif", Font.PLAIN, 14);
		bold14Font = new Font("Serif", Font.BOLD, 14);
		
		labelTxtPanel.setFont(plain14Font);
		brandLabel.setFont(plain14Font);
		modelLabel.setFont(plain14Font);
		weightLabel.setFont(plain14Font);
		priceLabel.setFont(plain14Font);
		OSLabel.setFont(plain14Font);
		typeLabel.setFont(plain14Font);
		reviewLabel.setFont(plain14Font);
		featuresLabel.setFont(plain14Font);
		
		brandTxt.setFont(bold14Font);
		modelTxt.setFont(bold14Font);
		weightTxt.setFont(bold14Font);
		priceTxt.setFont(bold14Font);
		
		cancelButton.setForeground(Color.RED);
		okButton.setForeground(Color.BLUE);
	
		textreviewDisplay.setFont(new Font("Serif", Font.BOLD, 14));
	}
	
	public static void createAndShowGUI() {

		// Set the name of the window.
		MobileDeviceFormV4 MobileDeviceFormV5 = new MobileDeviceFormV5("Mobile Device Form V5");

		// Call methods to add components and set features of window.
		MobileDeviceFormV5.addComponents();
		MobileDeviceFormV5.addMenus();
		MobileDeviceFormV5.setFrameFeatures();

	}
	
	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		
	}

	protected void updateMenuIcon() {
		super.updateMenuIcon();
	}
	
	protected void addSubMenus() {
		super.addSubMenus();
		

	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();

			}
		});
	}

}
