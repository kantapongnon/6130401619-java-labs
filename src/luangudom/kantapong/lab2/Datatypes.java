/**
* This Java program that declare the following variable in the given types and conditions. The output of
*  the program is in the format
* "My name is " + String variable
* "My student ID  is " + String variable
* - A char variable with value from the first letter of my first name.
* - An integer variable with value from the last two digits of my ID number.
* - A long variable with value from the last two digits of my ID number.
* - A float variable with value from the last two digits of my ID number 
* and has the floating point of the first two digits of my ID number.
* - A double variable with value from the last two digits of my ID number
* and has the floating point of the first two digits of my ID number.
* 
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 21, 2019
* 
* */
package luangudom.kantapong.lab2;

public class Datatypes {
	public static void main (String[] args) {
		//declare the String[] type
		String name = "Kantapong Luangudom";
		String student_ID = "6130401619";
		//declare the char type (use String subroutine to get the first letter)
		String first_letter = name.substring(0,1);
		//declare the boolean type
		boolean compare = true;
		String last_ID = student_ID.substring(8, 10);
		//declare the integer type (in octal and hexadecimal number)
		int id_oct = 023;
		int id_hex = 0x13;
		//declare the long type 
		long id_long = 19;
		//declare the float type
		float id_float = 19.61f;
		//declare the double type
		double id_double = 19.61;
		
		System.out.println("My name is " + name);
		System.out.println("My student ID is " + student_ID);
		System.out.println(first_letter + " " + compare + " " + id_oct + " " + id_hex );
		System.out.println(id_long + " " + id_float  + " " + id_double);
	}
	
}
