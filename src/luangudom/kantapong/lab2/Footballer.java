/**
* This Java program that accepts three arguments: your favorite football player, the
*  football club that the player plays for, and the nationality of that player. The output of
*  the program is in the format
* "My favorite football player is " + <footballer name>
* "His nationality is " + <nationality>
* "He plays for " + <football club>
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 21, 2019
* 
* */

package luangudom.kantapong.lab2;

public class Footballer {

	public static void main(String[] args) {
		// check user's input if it's in correct form and order.
		if (args.length != 3) {
			//this one will occur if the input is in false form/order.
			System.err.println("Usage: <Player name> <Nationality> <Footbal club>");
		}else{
			//if user fills in the correct  input, the program will run these following codes.
			System.out.println(" My favorite football player is " + args[0]);
			System.out.println(" His nationality is " + args[1]);
			System.out.println(" He plays for " + args[2]);
		}

	}

}
