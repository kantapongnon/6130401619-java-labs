/**
* This Java program that compute how much money user has. 
* He/She will input the number of notes of 1,000 Baht, 500 Baht, 100 Baht and 20 Baht and this program will output 
* the total amount of money he/she has.
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 21, 2019
* 
* */

package luangudom.kantapong.lab2;

public class ComputeMoney {
	public static void main (String args[]) {
		//Check if user has inputs wrong arguments form.
		if (args.length != 4) {
			System.out.println("ComputeMoney <1,000 Baht> <500 Baht> <100 Baht> <20 Baht>");
		//If user has inputs right arguments, the amount of bank bills should turn 
		//from String type into double type.
		}else{
			double onethousand_bill = Double.parseDouble(args[0]);
			double fivehundred_bill = Double.parseDouble(args[1]);
			double onehundred_bill = Double.parseDouble(args[2]);
			double twenty_bill = Double.parseDouble(args[3]);
		//Calculate the total price of each bank bill types.	
		double thousand = 1000 * onethousand_bill;
		double fivehundred = 500 * fivehundred_bill;
		double onehundred = 100 * onehundred_bill;
		double twenty = 20 * twenty_bill;
		//Calculate the total price.
		double sum = thousand + fivehundred + onehundred + twenty;
		//Output total money.
		System.out.println("Total money is " + sum );
		}
	
	}

}
		
			 
			 
			
