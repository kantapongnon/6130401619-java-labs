/**
* This Java program that declare that accepts one input argument (your school name) and then display the output.
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 21, 2019
* 
* */
package luangudom.kantapong.lab2;

public class StringAPI {
	public static void main(String[] args) {
		//Getting input from user (only 1 argument). it must contains the name of your school.
		String schoolName = args[0];
		
		//Split words with space to find the last word.
		String [] split_name =  schoolName.split(" ");
		
		//Determined by the last word to compare the next String type.
		for (int index = split_name.length-1 ; index >= split_name.length-1; index -= 1) {
	    	String lastword_school = split_name[index];
	    
	    //Make the last word into lower case to compare it easier.
	    String lowerlastword_name = lastword_school.toLowerCase();
	    
	    //Compare the word we want, then print the equals word to sort out  the type of the school.
	    if (lowerlastword_name.equals("university")) {
	    	System.out.println(schoolName + " is a " + lowerlastword_name);
	    } else if (lowerlastword_name.equals("college"))  {
	    	System.out.println(schoolName + " is a" + lowerlastword_name);
	    } else {
	    	System.out.println(schoolName + " is neither a university nor a college" );
	    
	    }
	    
	    }
	
	}

}