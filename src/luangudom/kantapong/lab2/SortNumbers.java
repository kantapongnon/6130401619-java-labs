/**
* This Java program that accepts five decimals from user, read and store them in an array, 
* and then use Java subroutine to sort them.
*
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 21, 2019
* 
* */

package luangudom.kantapong.lab2;
// Import Arrays class to use it to stored input numbers from user.
import java.util.Arrays;
public class SortNumbers {
	public static void main (String[] args) {
		//Convert these numbers form String to float.
		float number1 = Float.parseFloat(args[0]);
		float number2 = Float.parseFloat(args[1]);
		float number3 = Float.parseFloat(args[2]);
		float number4 = Float.parseFloat(args[3]);
		float number5 = Float.parseFloat(args[4]);
		// Put them into Array.
		float [] number_array = {number1, number2, number3, number4, number5};
		//Sort them from minimum to maximum. 
		Arrays.sort(number_array);
		//Print them from index 0 to index 4.
		for (int index = 0; index < number_array.length; index += 1) {
		     System.out.print(number_array[index] + " " );
		}
	
	}

}
