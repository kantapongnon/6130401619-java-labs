package luangudom.kantapong.lab6;

/**
* This GUI program is to demonstrate how Java GUI work and show a window contains information fields, operation choices and
* two buttons  (OK and Cancel). 
* 
* Author:@author Kantapong Luang-udom
* ID: 613040161-9
* Sec: 2
* Date: March 2, 2019
**/

//import neccessary functions.
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

//This class is a subclass of MySimpleWindow.
public class MobileDeviceFormV1 extends MySimpleWindow {

	// Declare variables
	protected JPanel labelTxtPanel;
	protected JPanel allPanel;
	protected JPanel dividedPanel;
	protected JLabel brandLabel;
	protected JTextField brandTxt;
	protected JLabel modelLabel;
	protected JTextField modelTxt;
	protected JLabel weightLabel;
	protected JTextField weightTxt;
	protected JLabel priceLabel;
	protected JTextField priceTxt;
	protected JLabel OSLabel;
	protected JPanel osPanel;
	protected JRadioButton AndroidButton;
	protected JRadioButton iOSButton;

	// This is a constructor to set the name of window.
	public MobileDeviceFormV1(String title) {
		super(title);

	}

	public static void createAndShowGUI() {

		// Set the name of the window.
		MobileDeviceFormV1 MobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");

		// Call methods to add components and set features of window.
		MobileDeviceFormV1.addComponents();
		MobileDeviceFormV1.setFrameFeatures();

	}

	protected void addComponents() {

		// Call super class to add components as the following below.
		super.addComponents();

		// Create and add all of components.
		dividedPanel = new JPanel();
		labelTxtPanel = new JPanel();
		allPanel = new JPanel();
		osPanel = new JPanel();

		dividedPanel.setLayout(new GridLayout(5, 2));
		brandLabel = new JLabel("Brand Name:");
		modelLabel = new JLabel("Model Name:");
		weightLabel = new JLabel("Weight (k.g.):");
		priceLabel = new JLabel("Price (Baht):");
		OSLabel = new JLabel("Mobile OS:");
		brandTxt = new JTextField(15);
		modelTxt = new JTextField(15);
		weightTxt = new JTextField(15);
		priceTxt = new JTextField(15);
		AndroidButton = new JRadioButton("Android");
		iOSButton = new JRadioButton("iOS");

		dividedPanel.add(brandLabel);
		dividedPanel.add(brandTxt);

		dividedPanel.add(modelLabel);
		dividedPanel.add(modelTxt);

		dividedPanel.add(weightLabel);
		dividedPanel.add(weightTxt);

		dividedPanel.add(priceLabel);
		dividedPanel.add(priceTxt);

		dividedPanel.add(OSLabel);
		ButtonGroup group = new ButtonGroup();
		group.add(AndroidButton);
		group.add(iOSButton);
		osPanel.add(AndroidButton);
		osPanel.add(iOSButton);

		dividedPanel.add(osPanel);
		labelTxtPanel.add(dividedPanel);
		buttonPanel.add(cancelButton);
		buttonPanel.add(okButton);
		

		// Set the direction of any sub-panel in window.
		allPanel.setLayout(new BorderLayout());
		allPanel.add(labelTxtPanel, BorderLayout.NORTH);
		allPanel.add(buttonPanel, BorderLayout.SOUTH);
		add(allPanel);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}