package luangudom.kantapong.lab6;

/**
* This GUI program is to demonstrate how Java GUI work and show a window contains two buttons (OK and Cancel). 
*
* 
* Author:@author Kantapong Luang-udom
* ID: 613040161-9
* Sec: 2
* Date: March 2, 2019
**/

//import neccessary functions.
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

//This class is a subclass of JFrame.
public class MySimpleWindow extends JFrame {

	// Declare variables
	protected JPanel buttonPanel;
	protected JButton okButton;
	protected JButton cancelButton;

	// This is a constructor to set the name of window.
	public MySimpleWindow(String title) {
		super(title);

	}

	// This method is to create objects to show GUI window.
	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	// This method is to set features of window.
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// This method is to add all of components to GUI.
	protected void addComponents() {
		// create and add components.
		buttonPanel = new JPanel();
		okButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		buttonPanel.add(cancelButton);
		buttonPanel.add(okButton);
		add(buttonPanel);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
