package luangudom.kantapong.lab6;

/**
* This GUI program is to demonstrate how Java GUI work and show a window contains information fields,type combo box, operation choices,
* two buttons (OK and Cancel),features list and review filed. It's also has a File menu and Config menu for using.
* 
* Author:@author Kantapong Luang-udom
* ID: 613040161-9
* Sec: 2
* Date: March 2, 2019
**/

//import neccessary functions.
import java.awt.*;
import javax.swing.*;

//This class is a subclass of MobileDeviceFormV2.
public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	// Declare variables
	protected JMenuBar menuBar;
	protected JMenu fileMenu;
	protected JMenu configMenu;
	protected JMenuItem newItem;
	protected JMenuItem openItem;
	protected JMenuItem saveItem;
	protected JMenuItem exitItem;
	protected JMenuItem colorItem;
	protected JMenuItem sizeItem;
	protected JPanel featuresPanel;
	protected JPanel reviewPanel;
	protected JPanel textandButtonPanel;
	protected JLabel featuresLabel;
	protected String[] featuresArray = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };
	protected JList textfeaturesDisplay;

	// This is a constructor to set the name of window.
	public MobileDeviceFormV3(String title) {
		super(title);

	}

	public static void createAndShowGUI() {
		// Set the name of the window.
		MobileDeviceFormV3 MobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3");

		// Call methods to add components and set features of window.
		MobileDeviceFormV3.addComponents();
		MobileDeviceFormV3.addMenus();
		MobileDeviceFormV3.setFrameFeatures();

	}

	protected void addComponents() {

		// Call super class to add components as the following below.
		super.addComponents();

		// Create and add all of components.
		featuresPanel = new JPanel();
		reviewPanel = new JPanel();
		textandButtonPanel = new JPanel();
		textandButtonPanel.setLayout(new GridLayout(2, 1));
		featuresPanel.setLayout(new GridLayout(1, 2));

		featuresLabel = new JLabel("Features:");
		textfeaturesDisplay = new JList(featuresArray);

		featuresPanel.add(featuresLabel);
		featuresPanel.add(textfeaturesDisplay);
		featuresPanel.add(reviewLabel);

		// Sort all of sub-panel and set the direction of it.
		textandButtonPanel.setLayout(new BorderLayout());
		textandButtonPanel.add(reviewLabel, BorderLayout.NORTH);
		textandButtonPanel.add(textScroll, BorderLayout.CENTER);
		textandButtonPanel.add(buttonPanel, BorderLayout.SOUTH);

		allPanel.add(labelTxtPanel, BorderLayout.NORTH);
		allPanel.add(featuresPanel, BorderLayout.CENTER);
		allPanel.add(textandButtonPanel, BorderLayout.SOUTH);

	}
	
	protected void addMenus() {
		
		// Create and add all of components.
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		configMenu = new JMenu("Config");
		newItem = new JMenuItem("New");

		openItem = new JMenuItem("Open");
		saveItem = new JMenuItem("Save");
		exitItem = new JMenuItem("Exit");
		colorItem = new JMenuItem("Color");
		sizeItem = new JMenuItem("Size");

		fileMenu.add(newItem);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);
		fileMenu.add(exitItem);

		configMenu.add(colorItem);
		configMenu.add(sizeItem);
		menuBar.add(fileMenu);
		menuBar.add(configMenu);
		setJMenuBar(menuBar);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	

}
