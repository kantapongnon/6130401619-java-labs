package luangudom.kantapong.lab6;

/**
* This GUI program is to demonstrate how Java GUI work and show a window contains information fields,type combo box, operation choices,
* two buttons (OK and Cancel) and review filed. 
* 
* Author:@author Kantapong Luang-udom
* ID: 613040161-9
* Sec: 2
* Date: March 2, 2019
**/

//import neccessary functions.
import java.awt.*;
import javax.swing.*;

//This class is a subclass of MobileDeviceFormV1.
public class MobileDeviceFormV2 extends MobileDeviceFormV1 {

	// Declare variables
	protected JPanel reviewPanel;
	protected JLabel typeLabel;
	protected JLabel reviewLabel;
	protected JComboBox typeBox;
	protected String[] deviceType = { "Phone", "Tablet", "Smart TV" };
	protected JTextArea textreviewDisplay;
	protected JScrollPane textScroll;
	protected String textReview = "Bigger than previous Note phones in" + " every way, the Samsung Galaxy Note "
			+ "9 has a larger 6.4-inch screen,heftier 4,000mAh battery, and a massive 1TB of storage option."
			+ "                                                                                              ";
	// This is a constructor to set the name of window.

	public MobileDeviceFormV2(String title) {
		super(title);

	}

	public static void createAndShowGUI() {

		// Set the name of the window.
		MobileDeviceFormV2 MobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2");

		// Call methods to add components and set features of window.
		MobileDeviceFormV2.addComponents();
		MobileDeviceFormV2.setFrameFeatures();

	}

	protected void addComponents() {

		// Call super class to add components as the following below.
		super.addComponents();

		// Create and add all of components.
		dividedPanel.setLayout(new GridLayout(7, 2));
		textreviewDisplay = new JTextArea(3, 29);
		reviewPanel = new JPanel();
		typeLabel = new JLabel("Type:");
		reviewLabel = new JLabel("Review:");
		typeBox = new JComboBox(deviceType);

		// Set editable for user to edit this combo box.
		typeBox.setEditable(true);
		dividedPanel.add(typeLabel);
		dividedPanel.add(typeBox);
		dividedPanel.add(reviewLabel);

		textreviewDisplay.setLineWrap(true);
		textreviewDisplay.setWrapStyleWord(true);
		textreviewDisplay.setText(textReview);
		textScroll = new JScrollPane(textreviewDisplay);

		reviewPanel.add(textScroll);

		// Set the direction of any sub-panel in window.
		allPanel.add(reviewPanel, BorderLayout.CENTER);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
