package luangudom.kantapong.lab11;
/**
* This Java program inherits from MobileDeviceFormV10. It is the mobile device form that user needs to fills all information in the blank.
* After user pressed OK button it will display the information of the mobile phone or it will reset itself if user pressed Cancel button.
* This version has been upgraded to check if weight and model name are correctly entered.
*
* @author Kantapong Luangudom
* ID: 613040161-9
* Sect: 2
* Date: May 7, 2019
*/


import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV11 extends MobileDeviceFormV10 {

	//Declared all essential variables.
	private static final long serialVersionUID = 1L;
	protected final int MIN_WEIGHT = 100;
	protected final int MAX_WEIGHT = 3000;

	public MobileDeviceFormV11(String title) {
		super(title);

	}

	protected void handleOKButton() {
		checkModelNameAndWeight();

	}
	
	//This method is use to check if weight and model name are correctly entered.
	protected void checkModelNameAndWeight() {
		//Check if user input model name or not.
		if (modelTxt.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Please enter model name");

		}
		
		//Check if weight is enter. If so, check that the weight is not too light or heavy.
		if (weightTxt.getText() != null) {

			try {
				int mobileWeight = Integer.parseInt(weightTxt.getText());
				String weightRange = "[" + MIN_WEIGHT + ", " + MAX_WEIGHT + "]";
				
				if (mobileWeight < MIN_WEIGHT) {
					JOptionPane.showMessageDialog(null,
							"Too light: valid weight is " + weightRange);

				} else if (mobileWeight > MAX_WEIGHT) {
					JOptionPane.showMessageDialog(null,
							"Too heavy: valid weight is " + weightRange);
				} else {
					//If everything is done, continue the process of program.
					super.handleOKButton();
				}

			
			} catch (NumberFormatException e) {
				//Preventing user to not enter string instead of numeric. 
				JOptionPane.showMessageDialog(null, "Please enter only numeric input for weight");
			}

		}

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV11 MobileDeviceFormV11 = new MobileDeviceFormV11("Mobile Device Form V11");
		MobileDeviceFormV11.addComponents();
		MobileDeviceFormV11.addMenus();
		MobileDeviceFormV11.setFrameFeatures();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
