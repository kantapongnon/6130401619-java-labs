package luangudom.kantapong.lab11;
/**
* This Java program inherits from MobileDeviceFormV9. It is the mobile device form that user needs to fills all information in the blank.
* After user pressed OK button it will display the information of the mobile phone or it will reset itself if user pressed Cancel button.
* This version includes display feature to display all information, sort feature to display all mobile information from the lowest to the highest 
* price and remove feature to remove some information. 
*
* @author Kantapong Luangudom
* ID: 613040161-9
* Sect: 2
* Date: May 7, 2019
* 
* */
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import luangudom.kantapong.lab10.MobileDeviceFormV9;

import luangudom.kantapong.lab5.MobileDevice;

public class MobileDeviceFormV10 extends MobileDeviceFormV9 {
	
	//Declared all essential variables.
	 
	private static final long serialVersionUID = 1L;
	protected ArrayList<MobileDevice> information = new ArrayList<MobileDevice>();
	protected static MobileDevice mobile;
	protected JMenu dataMenu;
	protected JMenuItem displayItem = new JMenuItem("Display");
	protected JMenuItem sortItem = new JMenuItem("Sort");
	protected JMenuItem searchItem = new JMenuItem("Search");
	protected JMenuItem removeItem = new JMenuItem("Remove");

	public MobileDeviceFormV10(String title) {
		super(title);

	}

	protected void handleOKButton() {
		super.handleOKButton();
		addMobileDevice();

	}

	protected void addMenus() {
		super.addMenus();

		dataMenu = new JMenu("Data");

		dataMenu.add(displayItem);
		dataMenu.add(sortItem);
		dataMenu.add(searchItem);
		dataMenu.add(removeItem);
		menuBar.add(dataMenu);

	}
	
	/**This method receiving input from user, send it to mobileDevice's constructor and put it in
	* an ArrayList.
	*/
	protected void addMobileDevice() {
		String ModelName = modelTxt.getText();
		String OS = receiveOS.getText();
		int Weight = Integer.parseInt(weightTxt.getText());
		int Price = Integer.parseInt(priceTxt.getText());
		mobile = new MobileDevice(ModelName, OS, Price, Weight);
		information.add(mobile);
		System.out.println(information);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV10 MobileDeviceFormV10 = new MobileDeviceFormV10("Mobile Device Form V10");
		MobileDeviceFormV10.addComponents();
		MobileDeviceFormV10.addMenus();
		MobileDeviceFormV10.setFrameFeatures();

	}
	
	//A method that use to display all information.
	protected void displayMobileDevices() {

		String inform = "";
		for (int i = 0; i < information.size(); i++) {
			String text = i + 1 + ": " + information.get(i).toString() + "\n";
			inform += text;

		}

		JOptionPane.showMessageDialog(null, inform);

	}
	
	//Use this class to compare two mobile price.
	class PriceComparator implements Comparator<MobileDevice> {

		@Override
		public int compare(MobileDevice o1, MobileDevice o2) {
			int price1 = ((MobileDevice) o1).getPrice();
			int price2 = ((MobileDevice) o2).getPrice();

			if (price1 > price2) {
				return 1;

			} else if (price1 < price2) {
				return -1;

			} else {
				return 0;
			}
			
			/**
			 * @return Integer to compare which price is higher
			 * 
			 */

		}
		

	}
	
	//A method that use to display all mobile information from the lowest price to the highest price.
	protected void sortMobileDevices() {
		Collections.sort(information, new PriceComparator());
		displayMobileDevices();
	}
	
	//A method that use to check if a mobile device is exists or not.
	protected void searchMobileDevice() {
		String searchInput = JOptionPane.showInputDialog("Please input model name to searchItem: ");
		String searchResult = "";
		
		
		for (int i = 0; i < information.size(); i++) {
			
			//Check the equality of user input and existing mobile device.
			if (searchInput.equalsIgnoreCase(information.get(i).getmodelName())) {
				searchResult += information.get(i) + " is found";

			}
			
			//If those two are equal, display "found". If not, display "NOT found".
			if (searchResult.length() != 0) {
				JOptionPane.showMessageDialog(null, searchResult);
			} else {
				searchResult += searchInput + " is NOT found";
				JOptionPane.showMessageDialog(null, searchResult);
			}
		}
	}
	
	//A method that use to remove an existing mobile device from an ArrayList. 
	protected void removeMobileDevice() {
		String removeInput = JOptionPane.showInputDialog("Please input model name to removeItem");
		String removeResult = "";
		
		//Check the equality of user input and existing mobile device.
		for (int i = 0; i < information.size(); i++) {
			if (removeInput.equalsIgnoreCase(information.get(i).getmodelName())) {
				removeResult += information.get(i) + " is removed";
				information.remove(i);
			}
			
			//If those two are equal,remove that mobile and display "removed". If not, do nothing and display "NOT found".
			if (removeResult.length() != 0) {
				JOptionPane.showMessageDialog(null, removeResult);
			} else {
				removeResult += removeInput + " is NOT found";
				JOptionPane.showMessageDialog(null, removeResult);

			}
		}

	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);

		if (source == displayItem) {
			displayMobileDevices();
		} else if (source == sortItem) {
			sortMobileDevices();
		} else if (source == searchItem) {
			searchMobileDevice();
		} else if (source == removeItem) {
			removeMobileDevice();
		}

	}

	protected void addListeners() {
		super.addListeners();
		displayItem.addActionListener(this);
		sortItem.addActionListener(this);
		searchItem.addActionListener(this);
		removeItem.addActionListener(this);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
