package luangudom.kantapong.lab11;
/**
* This Java program inherits from MobileDeviceFormV10. It is the mobile device form that user needs to fills all information in the blank.
* After user pressed OK button it will display the information of the mobile phone or it will reset itself if user pressed Cancel button.
* This version can save and open file.
*
* @author Kantapong Luangudom
* ID: 613040161-9
* Sect: 2
* Date: May 9, 2019
*/

import java.awt.event.ActionEvent;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV12 extends MobileDeviceFormV11 {

	
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV12(String title) {
		super(title);

	}

	protected void handleOKButton() {
		super.handleOKButton();

	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		if (source == saveItem) {
			handleSaveItem();

		} else if (source == openItem) {
			handleOpenItem();

		}

	}

	// A method for handle save option.
	protected void handleSaveItem() {
		// Check if approve option is true (0 means save).
		if (JFileChooser.APPROVE_OPTION == 0) {
			//Use FileOutput and ObjectOutput to write an object to file.
			try {

				File MobilesDevices = fc.getSelectedFile();
				FileOutputStream fileOutput = new FileOutputStream(MobilesDevices.getAbsolutePath());
				ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);

				for (Object mobile : information) {
					objectOutput.writeObject(mobile);

				}
				objectOutput.close();
				fileOutput.close();

			} catch (Exception e) {
				e.printStackTrace(System.err);
			}

		}

	}
	// A method for handle open option.
	protected void handleOpenItem() {
		// Check if approve option is true (0 means open).
		if (JFileChooser.APPROVE_OPTION == 0) {
			//Use FileInput and ObjectInput to read an object from file.
			try {
				File MobilesDevices = fc.getSelectedFile();
				FileInputStream fileInput = new FileInputStream(MobilesDevices.getAbsolutePath());
				ObjectInputStream objectInput = new ObjectInputStream(fileInput);

				String displayMobiles = "";

				try {

					while (true) {
						Object readObject = objectInput.readObject();
						String read = readObject.toString();
						if (read == null) {
							break;
						} else {
							displayMobiles += read + "\n";
						}

					}
					
					
				//When reach the end of file. The program will notify at console and show all information.
				} catch (EOFException e) {
					System.out.println("End");
					
				} finally {
					JOptionPane.showMessageDialog(null, displayMobiles);
				}

				objectInput.close();
				fileInput.close();

			} catch (IOException e) {
				e.printStackTrace(System.err);

			} catch (ClassNotFoundException e) {
				e.printStackTrace(System.err);
			} 
		}

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV12 MobileDeviceFormV12 = new MobileDeviceFormV12("Mobile Device Form V12");
		MobileDeviceFormV12.addComponents();
		MobileDeviceFormV12.addMenus();
		MobileDeviceFormV12.setFrameFeatures();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
