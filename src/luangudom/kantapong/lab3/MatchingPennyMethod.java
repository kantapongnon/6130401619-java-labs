/**
* This Java program that accepts only one scanner: penny's sides head or tail only.
* you'll win if your guess is the right one or you'll lose if not. 
* 
* The program is in the format:
*" Enter head or tail: " <input head, tail or exit>.
*" You plays " <user's input> .
*" Computer plays " <computer's input>.
*" You win. " or " Computer win. " depends on user's guess if user input the right one or not.
*" Incorrect input. head or tail only. " error message if user input anything else of "head" or "tail".
*" Bye. " if user input "exit".
* 
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 28, 2019
*
**/

/**
 * Matching penny game by java programming (Defined methods).
 * @author Kantapong Luangudom
 * 
 */
 

package luangudom.kantapong.lab3;
//Import Scanner subroutines for comfortably use when write the program.
import java.util.Scanner;

public class MatchingPennyMethod {
	//Define String "humanChoice" to accept input from user and 
	//"compChoice" to generate choice of computer (head or tail). 
	  
	static String humanChoice;
	static String compChoice;
	
	// Define "checker"  boolean variable type to use instead of continue statement. Because if we use continue 
	// statement it will continue the loop too quick and cause the output (error case display) into problem.  
	// The error output line displaying will also in uncontrollable form. 
	 static boolean checker = true;
	
	public static void main (String[] args) {
		//Use forever loop to continue the game until this program get "exit" from user.
		while ( true )
		{
		 //Call function by  output.
		 System.out.println( "You play " + acceptInput() );
		 System.out.println( "Computer play " + genComChoice() );
		 System.out.println( displayWinner() );
		
		}
		 
	}
	
	public static String acceptInput() {
		 /**
	     * Accept user's input (head, tail or exit).
	     * @return humanChoice
	     */
			//Use forever loop to continue the game until this program get "exit" from user.
			while ( true )
			{	
				checker = true;
					// Define a scanner to get input from user.
				System.out.print( "\nEnter Head or Tail: " ); 
				Scanner play = new Scanner( System.in );
				
					//Convert scanner to String type and get rid of case sensitive.
				String play_string = play.nextLine();
				final String  play_lower = play_string.toLowerCase();
			
				
					//This will occur if user input "head".
				if ( play_lower.equals( "head" ) ) {
						// in this case, human choice equals to "head" and return to use in the future.
						humanChoice = play_lower;
						return humanChoice;
					
				
					//This will occur if user input "tail".
				} else if ( ( play_lower.equals( "tail" ) ) ) {
						// in this case, human choice equals to "tail" and return to use in the future.
						humanChoice = play_lower;
						return humanChoice;
					
				
					//The program will shut itself  completely if player input "exit".
				} else if ( play_lower.equals( "exit" ) ) {
					System.out.println( "Bye." );
					//Close scanner.
					play.close();
					System.exit(0);
					
					//This will occur if user input anything else of "head" or "tail".
				} else {
					System.err.print( "\nIncorrect input. head or tail only." );
					//As mentioned above. if the program run into this error case, the boolean "checker" will  
					//change into false and do the same thing as continue statement.
					checker = false;
				}
				
			} 
			
	}
	
	public static String genComChoice() {
		 /**
	     * Generate computer choice from computer.
	     * @return genComChoice.
	     */
		//Get random integer (0 and 1) from computer.
		int random_match = (int)(Math.random() * 2);
			if ( random_match == 0 && checker == true ) {
				//This will occur if random integer equals to 0. Define the computer to play "tail".
				compChoice = "tail";
			} else if  ( random_match == 1 && checker == true ) {
				//This will occur if random integer equals to 1. Define the computer to play "head".
				compChoice = "head";
				
			}
			
			return compChoice;
			
	}
	
	
			
		
	public static String displayWinner() {
		 /**
	     * Output the result.
	     * @return Win_message.
	     */
		// if user's choice equals to computer choice the program will display "You win". if not, it will display "Computer win"
		if ( humanChoice.equals(compChoice) ) {
			String Win_message = "You win." ;
			return Win_message ;
		} else {
			String Win_message = "Computer Win." ;
			return Win_message ;
			
		}
		
	}

}


		
	
		
					

	


