/**
 * Java program for indicates how fast is user can type .
 * @author Kantapong Luangudom
 * 
 */
/**
* This Java program that  test whether a user types faster or slower than an average person. 
* An average person types at the speed of 40 words per minute. The program random 8 colors from a list of rainbow colors 
* (RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET). Color can be picked more than once. A user must type in the same list ignoring case. 
* If the user type in incorrectly, he needs to type in again. 
* After the correct input is enter, the time used for typing in will be calculated and if he/she types in faster than or equal to 12 seconds, 
* he types faster than average otherwise he/she types slower than average.
* The program is in the format:
*"  <randomed colors>.
*" Type your answer " <user's input + "\n"> .
*" Your time is " <computer's output>.
*" You type slower than average person. " or "You type slower than average person. " 
*depends on user's timing (slower than 12 seconds or faster than 12 seconds).
*
* 
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 28, 2019
*
**/

/**
 * Java program for indicates how fast is user can type .
 * @author Kantapong Luangudom
 * 
 */

package luangudom.kantapong.lab3;
//Import all of subroutines for comfortably use when write the program.
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.lang.*;
public class TypingTest {
	
	
	 
	 public static void main (String[] args) throws InterruptedException {
		 //Defined String colors array to random from it (can duplicate).
		 String[] colors = {"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET" };
		 //Use while loop to pick a color and add it to another String array until it's full with 8 colors (can duplicate).
		 int i = 0;
		 String[] colors_random = new String[8];
		 while ( i < 8 )
		 { 
			 
			 String randomcolor = getRandom(colors);
			 colors_random[i] = randomcolor;
			 i++;
		 
		 }
		 //Use for loop to output colors user need to type in.
		 for ( int j = 0; j < 8; j++ ) {
			 System.out.print(colors_random[j] + " ");
		 }
		 /**
		  * Java program for indicates how fast is user can type .
		  * @author Kantapong Luangudom
		  * 
		  */
		 //Start timing from here.
		 long start = System.currentTimeMillis();
		 //Use forever loop in case user type in an incorrect or misspell each colors. 
		 while (true)
		 {
			 
			 System.out.print("\nType your answer: ");
			 //Use scanner method to get input from user. Convert it to String and get rid of sensitive case and put it into an array by using split method.
			 Scanner answer = new Scanner(System.in);
			 String answer_String = answer.nextLine();
			 String answer_upper = answer_String.toUpperCase();
			 String [] answer_array = answer_upper.split(" ");
			 //Check if user type in all 8 elements or not first.
			 if (answer_array.length != 8) {
				 //If user type in not complete the program will continue looping.
				 continue ;
			// If user type in all 8 elements and correct, the  timing will end here.
			 } else if (Arrays.equals(answer_array, colors_random)) {
				 long end = System.currentTimeMillis();
				 //Calculate the elapsed time. 
				 float elapsedTimeSec = (end-start)/1000F ;
				 //Output user's total time.
				 System.out.println("Your time is " + elapsedTimeSec);
				 if (elapsedTimeSec <= 12.000F ) {
					 //This will occur if user type faster than 12 seconds. 
					 System.out.println("You type faster than average person.");
				 } else if (elapsedTimeSec > 12.000F )  {
					//This will occur if user type slower than 12 seconds. 
					 System.out.println("You type slower than average person.");
				 }
				 break ;
			// If user type in all 8 elements but incorrect, the program will continue looping.
			 } else {
				 continue ;
			 }
				 
		 }

	 }
	 /**
	  * Java program for indicates how fast is user can type .
	  * @author Kantapong Luangudom
	  * 
	  */
	 //Defined getRandom method to pick colors user need to type in from an array (8 colors).
	 private static String getRandom(String[] array) {
		    int colors = new Random().nextInt(array.length);
		    return array[colors] ;
		    /**
		     * Get random colors from an array.
		     * @param colors[]
		     * @return array[colors]
		     */
	
	}
	 
}
/**
 * Java program for indicates how fast is user can type .
 * @author Kantapong Luangudom
 * 
 */

