/**
* This Java program that find the minimum, the maximum, the average and the standard deviation of the list of numbers entered. 

* The first argument is how many numbers to be entered. The rest of the program arguments are list of numbers.
* 
* The program is in the format:
*" Max is <max value number> Min is <min value number>
*" Average is " <average value calculated by the program> .
*" Standard Deviation is " <Standard deviation value calculated by the program>.
*" <BasicStat> <numNumbers> <numbers>... Error message when input is incorrect.
* 
* Author: Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 28, 2019
*
**/

/**
 * A java program for calculating statistics value.
 * @author Kantapong Luangudom
 * 
 */

package luangudom.kantapong.lab3;
//Import Arrays subroutines for comfortably use when write the program.
import java.util.Arrays;

public class BasicStat {
	//Define an Integer "numInput" to accept amount of numbers to calculate and
    //an Arrays "num[]" to accept each numbers to calculate.
	public static int numInput ;
	public static double num[];
			
	public static void main (String[] args) {
			//Call acceptInput function to check if the number of entered 
			//numbers is the same as the first integer entered.
			acceptInput(args);
			//Call displayStats function to output the result.
			System.out.println(displayStats(args));
	}
		
		//Defined acceptInput function to check if the number of entered 
		//numbers is the same as the first integer entered.
		public static double[] acceptInput(String[] args) {
			/**
			 * Accept arguments from input, convert it to Double and put it into an array.
			 * @param arguments
			 * @return num[]
			 */
			
			//Change the first argument to integer(amount of numbers to be calculate).
			int first_num = Integer.parseInt(args[0]);
			//Check whether the first integer is equals to amount of arguments behind it or not.
			if (first_num != (args.length-1)  ) {
				//This will occur if amount of arguments behind the first integer are unequal. 
				System.err.println("<BasicStat> <numNumbers> <numbers>...");
			} else {
				//Declared numInput to stored the first_num value and num[] to stored numInput value(amount of numbers to be calculate) plus with the first integer(+1).
				numInput = first_num;
				num = new double[numInput+1] ;
				// Use for loop to access all arguments, changed it to Double and stored it in num[].
				for (int i = 0; i <= numInput; i++) {
					double args_double = Double.parseDouble(args[i]);
					num[i] = args_double;
				}
				
			}
			//This method return Double array.
			return num ;
		
		}
		
		//Defined displayStats function to output the result (Maximum, Minimum, Average and Standard deviation).
		public static String displayStats(String[] args) {
			/**
			 *Calculate statistics value and output it.
			 * @param arguments
			 * @return display_result
			 */
			//declared num2[] to stored all of numbers to be calculate (get rid of the first integer).
			double[] num2 = new double[numInput];
			for (int i = 1; i <= numInput; i++  ) {
				num2[i-1] = num[i];
			}
			
			//Use Arrays.sort() method to sort numbers from minimum to maximum.
			Arrays.sort(num2);
			double max = num2[numInput-1] ;
			double min = num2[0] ;
			//Use for loop to access num2[] and calculate summation of numbers.
			double sum = 0;
			for (int i = 0; i < num2.length; i++ ) {
				sum += num2[i];
			}
			
			//Calculate the average value.
			double avg = sum/numInput;
			//Use for loop to access num2[] and calculate summation of (number-average)^2.
			double sum_sd = 0;
			for (int i = 0; i < num2.length; i++ ) {
				sum_sd += Math.pow((num2[i] - avg), 2);
				
			}
			
			//Calculate the standard deviation value.
			double standard_dev = Math.sqrt(sum_sd/numInput);
			
			//Defined String variable to return the output to main.
			String display_result = ("Max is " + max + " Min is " + min +"\n" 
			+"Average is " + avg +"\n" +"Standard Deviation is " + standard_dev) ;
			
			//This method return String.
			return display_result;
			
		}
	}



