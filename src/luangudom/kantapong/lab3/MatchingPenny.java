/**
* This Java program that accepts only one scanner: penny's sides head or tail only.
* you'll win if your guess is the right one or you'll lose if not. 
* 
* The program is in the format:
*" Enter head or tail: " <input head, tail or exit>.
*" You plays " <user's input> .
*" Computer plays " <computer's input>.
*" You win. " or " Computer win. " depends on user's guess if user input the right one or not.
*" Incorrect input. head or tail only. " error message if user input anything else of "head" or "tail".
*" Bye. " if user input "exit".
* 
* Author:  Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: January 28, 2019
*
**/

/**
 * Matching penny game by java programming.
 * @author Kantapong Luangudom
 * 
 */


package luangudom.kantapong.lab3;
//Import all of subroutines for comfortably use when write the program.
import java.util.*;

public class MatchingPenny {
	
	public static void main(String[] args) {
			//Use forever loop to continue the game until this program get "exit" from user.
	 		while (true)
			{	
	 			// Define "checker" boolean variable type to use instead of continue statement. Because if we use continue 
	 			// statement it will continue the loop too quick and cause the output (error case display) into problem.  
	 			// The error output line displaying will also in uncontrollable form. 
				boolean checker = true;
				// Define a scanner to get input from user.
				System.out.print("\nEnter Head or Tail: "); 
				Scanner play = new Scanner(System.in);
				//Convert scanner to String type and get rid of case sensitive.
				String play_string = play.nextLine();
				String play_lower = play_string.toLowerCase();
				
				//This will occur if user input "head".
				if ( play_lower.equals( "head" ) ) {
					System.out.println("You plays head.");
				//This will occur if user input "tail".
				} else if ( ( play_lower.equals( "tail" ) ) ) {
					System.out.println("You plays tail.");
				//The program will shut itself  completely if player input "exit".
				} else if ( play_lower.equals( "exit" ) ) {
					System.out.println("Bye.");
					//Close scanner.
					play.close();
					System.exit(0);
				//This will occur if user input anything else of "head" or "tail".
				} else {
					System.err.print( " Incorrect input. head or tail only. " );
					checker = false;
					//As mentioned above. if the program run into this error case, the boolean "checker" will  
					//change into false and do the same thing as continue statement.
					
				}
				//Get random integer (0 and 1) from computer.
				int random_match = (int)(Math.random() * 2);
				//Define the statement. The random integer from computer equals to 0 while the checker is still true.
				if ( random_match == 0 && checker == true) {
					//This will occur if random integer equals to 0. Define the computer to play "tail".
					System.out.println("Computer plays tail.");
					String computer_play_0 = "tail";
						//If user input equals to "tail",  "You win" will occur. If not, "Computer win" will occur instead.  
						if ( play_lower.equals( computer_play_0 ) ) {
							System.out.println("You win.");
						} else {
							System.out.println("Computer win.");
						}
				//Define the statement. The random integer from computer equals to 1 while the checker is still true.
				} else if (random_match == 1 && checker == true) {
					//This will occur if random integer equals to 1. Define the computer to play "head".
					System.out.println("Computer plays head.");
					String computer_play_1 = "head" ;
						//If user input equals to "head",  "You win" will occur. If not, "Computer win" will occur instead.
						if ( play_lower.equals( computer_play_1 ) ) {
							System.out.println("You win.");
						} else {
							System.out.println("Computer win.");
						
						}
				 
				}
			
		}	
		
	}

}





