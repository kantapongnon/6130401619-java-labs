package luangudom.kantapong.lab10;

/**
* This Java program inherits from MobileDeviceFormV7. It is the mobile device form that user needs to fill information in the blank.
* After user pressed OK button it will display the information of the mobile phone or it will reset itself if user pressed Cancel button.
* This version included accelerator key and mnemonic key for using already.
*
* @author Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: April 21, 2019
* 
* */

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 implements KeyListener {
	
	JMenuItem customItem = new JMenuItem("Custom ...");

	public MobileDeviceFormV8(String title) {
		super(title);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	
	protected void addSubMenus() {
		//Call superclass's sub menus. 
		super.addSubMenus();
		
		//Add listener to menu items and add custom menu.
		newItem.addKeyListener(this);
		color.add(customItem);
		
		//Set mnemonic key for all menu items.
		fileMenu.setMnemonic(KeyEvent.VK_F);
		newItem.setMnemonic(KeyEvent.VK_N);
		openItem.setMnemonic(KeyEvent.VK_O);
		saveItem.setMnemonic(KeyEvent.VK_S);
		exitItem.setMnemonic(KeyEvent.VK_X);

		configMenu.setMnemonic(KeyEvent.VK_C);
		color.setMnemonic(KeyEvent.VK_L);
		redSubmenu.setMnemonic(KeyEvent.VK_R);
		greenSubmenu.setMnemonic(KeyEvent.VK_G);
		blueSubmenu.setMnemonic(KeyEvent.VK_B);
		customItem.setMnemonic(KeyEvent.VK_U);

		//Set accelerator key for sub menu items.
		newItem.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		openItem.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		saveItem.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		exitItem.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));

		redSubmenu.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		greenSubmenu.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_G, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		blueSubmenu.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_B, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		customItem.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_U, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV8 MobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		MobileDeviceFormV8.addComponents();
		MobileDeviceFormV8.addMenus();
		MobileDeviceFormV8.setFrameFeatures();
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
}
