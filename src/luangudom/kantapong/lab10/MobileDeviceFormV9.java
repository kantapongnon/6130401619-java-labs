package luangudom.kantapong.lab10;

/**
* This Java program inherits from MobileDeviceFormV7. It is the mobile device form that user needs to fill information in the blank.
* After user pressed OK button it will display the information of the mobile phone or it will reset itself if user pressed Cancel button.
* This version included accelerator key, mnemonic key and many features such as open file, save file and exit program. 
*
* @author Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: April 21, 2019
* 
* */

import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import javafx.beans.value.*;

import java.awt.*;

import luangudom.kantapong.lab10.MobileDeviceFormV7.SharedListSelectionHandler;

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {
	protected JFileChooser fc = new JFileChooser();

	public MobileDeviceFormV9(String title) {
		super(title);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public void actionPerformed(ActionEvent event) {
		// Call superclass's action event.
		super.actionPerformed(event);

		// This is use to handle when user choose open menu.
		if (source == openItem) {
			int openVal = fc.showOpenDialog(MobileDeviceFormV9.this);

			// If user successfully opened file, the openVal will returns 0 then the message
			// of opening file will show.
			if (openVal == 0) {
				String fileName = (fc.getSelectedFile()).getName();
				JOptionPane.showMessageDialog(null, "Opening file " + fileName);

				// If user cancelled to open file, the openVal will returns 1 then the message
				// of cancelled opening file will show.
			} else if (openVal == 1) {
				JOptionPane.showMessageDialog(null, "Open command cancelled by user");

			}

			// This is use to handle when user choose save menu.
		} else if (source == saveItem) {
			int saveVal = fc.showSaveDialog(MobileDeviceFormV9.this);

			// If user successfully saved file, the saveVal will returns 0 then the message
			// of saving file will show.
			if (saveVal == 0) {
				String fileName = (fc.getSelectedFile()).getName();
				JOptionPane.showMessageDialog(null, "Saving file " + fileName);

				// If user cancelled to save file, the saveVal will returns 1 then the message
				// of cancelled saving file will show.
			} else if (saveVal == 1) {
				JOptionPane.showMessageDialog(null, "Save command cancelled by user");

			}
			
			// If user choose exit menu the program will end.
		} else if (source == exitItem) {
			System.exit(0);
			
			// If user choose red color menu the background of review panel will set to red color.
		} else if (source == redSubmenu) {

			textreviewDisplay.setBackground(Color.RED);
			
			// If user choose green color menu the background of review panel will set to
			// green color.
		} else if (source == greenSubmenu) {

			textreviewDisplay.setBackground(Color.GREEN);
			
			// If user choose blue color menu the background of review panel will set to
			// blue color.
		} else {

			textreviewDisplay.setBackground(Color.BLUE);

		}
	}

	protected void addSubMenus() {
		// Call superclass's sub menus.
		super.addSubMenus();

		// Add listener to all essential menu.
		openItem.addActionListener(this);
		saveItem.addActionListener(this);
		exitItem.addActionListener(this);
		redSubmenu.addActionListener(this);
		greenSubmenu.addActionListener(this);
		blueSubmenu.addActionListener(this);

		customItem.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				Color newColor = JColorChooser.showDialog(null, "Color setting", textreviewDisplay.getBackground());
				textreviewDisplay.setBackground(newColor);
			}

		});

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV9 MobileDeviceFormV9 = new MobileDeviceFormV9("Mobile Device Form V9");
		MobileDeviceFormV9.addComponents();
		MobileDeviceFormV9.addMenus();
		MobileDeviceFormV9.setFrameFeatures();

	}

}
