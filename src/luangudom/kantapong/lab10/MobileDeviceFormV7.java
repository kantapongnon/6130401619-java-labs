package luangudom.kantapong.lab10;

/**
* This Java program inherits from MobileDeviceFormV6. It is the mobile device form that user needs to fill information in the blank.
* After user pressed OK button it will display the information of the mobile phone or it will reset itself if user pressed Cancel button.
*
* @author Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: April 21, 2019
* 
* */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import luangudom.kantapong.lab6.MobileDeviceFormV3;
import luangudom.kantapong.lab7.MobileDeviceFormV6;

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener, ItemListener {
	// Declare all needed variables.
	protected int changeOsCounter = 0;
	protected int numSelectedFeatures = 0;

	protected String spec_OS;
	protected String type;
	protected String features;
	protected String review;
	protected ListSelectionModel featuresList;
	protected int initChoices[] = { 0, 1, 3 };
	protected JRadioButton receiveOS;
	protected Object source;

	public MobileDeviceFormV7(String title) {
		super(title);

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	//This class is use to check when user change the features. The numSelectedFeatures is use to prevent 
	//pop-up message of changing feature that might happen from the start of this program even user didn't change it.
	class SharedListSelectionHandler implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent event) {
			ListSelectionModel list = (ListSelectionModel) event.getSource();
			String receivechangeFeature = "";
			Object[] selected = textfeaturesDisplay.getSelectedValues();

			int numSelected = selected.length;
			features = "\n" + "Features: ";

			if (numSelectedFeatures >= 3) {
				if (selected.length == 1) {
					JOptionPane.showMessageDialog(null, selected);

				} else {
					for (int j = 0; j < numSelected; j++) {
						receivechangeFeature += selected[j];
						receivechangeFeature += ", ";
					}

					receivechangeFeature = receivechangeFeature.substring(0, receivechangeFeature.length() - 2);
					JOptionPane.showMessageDialog(null, receivechangeFeature);

				}

			}

			for (int i = 0; i < numSelected; i++) {

				if (i == numSelected - 1) {
					features = features + selected[i];

				} else {
					features = features + selected[i] + ", ";

				}
			}

			numSelectedFeatures += 1;

		}

	}

	public void actionPerformed(ActionEvent event) {
		source = event.getSource();
		featuresList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		// Receive the string of components to display.
		if (source == okButton) {
			String BrandName = brandTxt.getText();
			String ModelName = modelTxt.getText();
			String Weight = weightTxt.getText();
			String Price = priceTxt.getText();
			String OS = receiveOS.getText();
			spec_OS = "Brand Name: " + BrandName + ", Model Name: " + ModelName + ", Weight: " + Weight + ", Price: "
					+ Price + "\nOS: " + OS;
			review = "\nReview: " + textreviewDisplay.getText();
			type = "\nType: " + typeBox.getSelectedItem();
			handleOKButton();

			// Reset the form after user pressed Cancel button.
		} else if (source == cancelButton) {
			handleCancelButton();

		}

	}

	// Add listener to all essential button, list and combo box.
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		AndroidButton.addItemListener(this);
		iOSButton.addItemListener(this);
		typeBox.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, "Type is updated to " + typeBox.getSelectedItem());
			}

		});

		featuresList = textfeaturesDisplay.getSelectionModel();
		featuresList.addListSelectionListener(new SharedListSelectionHandler());
	}

	// Receive the state of OS if it changed by the user.
	public void itemStateChanged(ItemEvent event) {
		int change = event.getStateChange();
		receiveOS = (JRadioButton) event.getItemSelectable();

		if (change == ItemEvent.SELECTED) {
			spec_OS = spec_OS + "\nOS: " + receiveOS.getText();

			if (changeOsCounter >= 1) {
				JOptionPane.showMessageDialog(null, "Your os platform is now changed to " + receiveOS.getText());

			}
			changeOsCounter += 1;
		}

	}

	// Display the information after user pressed OK button.
	protected void handleOKButton() {
		JOptionPane.showMessageDialog(null, spec_OS + type + features + review);

	}

	// Reset to the blank form after user pressed Cancel button.
	private void handleCancelButton() {
		brandTxt.setText("");
		modelTxt.setText("");
		weightTxt.setText("");
		priceTxt.setText("");

	}

	// Call superclass's components, set default OS selection and set default
	// features.
	public void addComponents() {
		super.addComponents();
		addListeners();

		AndroidButton.setSelected(true);

		textfeaturesDisplay.setSelectedIndices(initChoices);

	}

	public static void createAndShowGUI() {
		MobileDeviceFormV7 MobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		MobileDeviceFormV7.addComponents();
		MobileDeviceFormV7.addMenus();
		MobileDeviceFormV7.setFrameFeatures();
	}

}
