/**
* This Java program records the specifications information of Toyota's car.

* Author:  @author Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: February 4, 2019
*
**/


package luangudom.kantapong.lab4;

//Make this class into subclass of Automobile class and implements two interfaces called Movable and Refuelable.
public class ToyotaAuto extends Automobile implements Movable, Refuelable{
	
	private int gasoline ;
	private int speed ;
	public  int maxSpeed ;
	private int acceleration ;
	public String model ;
	
	//Declared a constructor to set the default values of these following fields.
	public ToyotaAuto() {
		this.maxSpeed = 0;
		this.acceleration = 0;
		this.model = "car1";
		
	}
	
	//Declared a constructor to accepts values for the fields gasoline, speed, maxSpeed, acceleration, model, color.
	public ToyotaAuto(int maxSpeed, int acceleration, String model) {
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.gasoline = 100;
		
	}
	
	//Declared public methods to get each information.
	public void refuel() {
		this.gasoline = 100 ;
		System.out.println(model + " refuels"); 
		
	}
	
	public void accelerate() {
		this.gasoline = gasoline-15;
		this.speed += acceleration;
		if(speed > maxSpeed) {
			this.speed = maxSpeed ;
		}
		System.out.println(model + " accelerates" );
		
	}
	
	public void brake() {
		this.gasoline = gasoline-15;
		this.speed -= acceleration ;
		if (speed <= 0) {
			this.speed = 0;
		}
		System.out.println( model + " brakes");
		
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
		if (speed < 0) {
			this.speed = 0;
		}else if (speed > maxSpeed) {
			this.speed = maxSpeed;
		}
	}
	
	//Declared this method to return the max speed of the car to main method.
	public int getmaxSpeed() {
		return maxSpeed ;
		/**
		 * @return maxSpeed
		 */
	}
	
	
	public String toString() {
		return ( model + " gas:" + gasoline + " speed:" + speed 
				+ " max speed:" + maxSpeed + " acceleration:" + acceleration );
		/**
		* @return String
		*/
		
	}

}
