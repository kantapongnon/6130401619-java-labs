package luangudom.kantapong.lab4;

public interface Movable  {
	
	public void accelerate();
	
	public void brake();
	
	public void setSpeed(int speed);

}


