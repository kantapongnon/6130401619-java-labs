/**
* This Java program records the specifications information about automobile.
* The program is in the format:
Car 1's max speed is <maxSpeed>
Car 2's max speed is <maxSpeed>
Car x max speed has increased to <setmaxSpeed>
There are <numberOfAutomobile> automobile
Now there are <numberOfAutomobile> automobile
Automobile [ gasoline = gasoline, speed = speed, acceleration = acceleration, maxSpeed = maxSpeed, model = model, color = color]
Automobile [ gasoline = 0, speed = 0, acceleration = 0, maxSpeed = 160, model = Automobile, color = WHITE]

* Author:  @author Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: February 4, 2019
*
**/


package luangudom.kantapong.lab4;


public class Automobile {
		//Declared variables to record each of specifications information.
		private int gasoline ;
		private int speed ;
		private int maxspeed ;
		private int acceleration ;
		private String model ;
		private Color color ;
		private static int numberOfAutomobile = 0 ;
		public enum Color {
			RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE, BLACK
		}
		
		
		//Declared a constructor to accepts values for the fields gasoline, speed, maxSpeed, acceleration, model, color.
		public Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
			this.gasoline = gasoline;
			this.speed = speed;
			this.acceleration = acceleration;
			this.maxspeed = maxSpeed;
			this.model = model;
			this.color = color ;
			//When user calls this constructor a new automobile has been add.
			numberOfAutomobile += 1 ;
			
		}
		
		//Declared a constructor to set the default values of each fields.
		public Automobile() {
			gasoline = 0 ;
			speed = 0 ;
			acceleration = 0 ;
			maxspeed = 160 ;
			model = "Automobile";
			color = Color.WHITE ;
			numberOfAutomobile += 1 ;
				
		}
		
		//Override this method to display all of the information
		public String toString() {
			return  "Automobile " + "[ gasoline = " + gasoline + ", " + "speed = " + speed + ", " 
					+ "acceleration = " + acceleration + ", " +  "maxSpeed = " + maxspeed + ", " + "model = " 
					+ model +", " + "color = " + color + "]";
					/**
					 * @return String
					 */
		}
		
		//Declared public methods to set and get each information.

		public void setgasoline(int gasoline) {
			this.gasoline = gasoline;
		}
		
		public int getsaoline() {
			return gasoline;
		}
		
		public void setspeed(int speed) {
			this.speed = speed;
		}
		
		public int getspeed() {
			return speed;
		}
		
		public void setmaxSpeed(int maxSpeed) {
			this.maxspeed = maxSpeed;
		}
		
		public int getmaxSpeed() {
			return maxspeed;
		}
		
		public void setacceleration(int acceleration) {
			this.acceleration = acceleration;
		}
		
		public int getacceleration() {
			return acceleration;
		}
		
		public void setmodel (String model) {
			this.model = model;
		}
		
		public String getmodel() {
			return model;
		}
		
		public void setcolor (Color color) {
			this.color = color;
		}
		
		public Color getcolor() {
			return color;
		}
		
		//Declared this method to return the number of automobile to main method.
		public static int getnumberOfAutomobile() {
			return numberOfAutomobile;
			/**
			 * @return numberOfAutomobile
			 */
		}

}
