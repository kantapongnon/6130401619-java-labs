/**
* This Java program displays the specifications information about automobile.
* The program is in the format:
<car x> gas: <gasoline> speed: <speed> max speed: <maxSpeed> acceleration: <acceleration>
<car y> gas: <gasoline> speed: <speed> max speed: <maxSpeed> acceleration: <acceleration>
<car x or car y> accelerates,brakes or refuels (depends on methods calling.)

(These following depend on maxSpeed value.)
<car x> is NOT faster than <car y>
<car y> is faster than <car x>


* Author:  @author Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: February 4, 2019
*
**/

package luangudom.kantapong.lab4;


//Import all of elements in these two classes to display the information.
import luangudom.kantapong.lab4.ToyotaAuto.*;
import luangudom.kantapong.lab4.HondaAuto.*;

public class TestDrive2  {
	
	public static void main(String[] args) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.accelerate();
		car2.accelerate();
		
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.brake();
		car1.brake();
		car2.brake();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);
		isFaster(car1, car2);
		isFaster(car2, car1);
	}
	
	
	//Declared this method to comparing which car is faster.
	public static void isFaster(ToyotaAuto car1, HondaAuto car2) {
		if ( car1.maxSpeed > car2.maxSpeed) {
			System.out.println(car1.model + " is faster than " + car2.model);
		} else {
			System.out.println(car1.model + " is NOT faster than " + car2.model);
		}
		
	
	}
	
	//It's the same method as above but use in case car swapped (for example: isFaster(car1, car2) and then swap it to isFaster(car2, car1).
	public static void isFaster(HondaAuto car2, ToyotaAuto car1) {
		if ( car1.maxSpeed > car2.maxSpeed) {
			System.out.println(car2.model + " is NOT faster than " + car1.model);
			
		} else {
			
			System.out.println(car2.model + " is faster than " + car1.model);
		}
		
	}

}


