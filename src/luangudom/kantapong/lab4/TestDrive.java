/**
* This Java program displays the specifications information about automobile.

* Author:  @author Kantapong Luangudom
* ID: 613040161-9
* Sec: 2
* Date: February 4, 2019
*
**/


package luangudom.kantapong.lab4;

//make this class into a subclass of Automobile class. 
public class TestDrive extends Automobile{
	
	public static void main(String[] args) {
		//Call a function to records the specifications and display it.
		Automobile car1 = new Automobile(100, 0, 200, 0, "Sport Car", Color.RED) ;
		Automobile car2 = new Automobile(100, 0, 100, 0, "Electric Car", Color.WHITE) ;
		System.out.println("Car 1's max speed is " + car1.getmaxSpeed());
		System.out.println("Car 2's max speed is " + car2.getmaxSpeed());
		car1.setmaxSpeed(280) ;
		System.out.println("Car 1's max speed has increased to " + car1.getmaxSpeed());
		System.out.println("There are " + Automobile.getnumberOfAutomobile() + " automobile");
		Automobile car3 = new Automobile();
		System.out.println("Now there are " + Automobile.getnumberOfAutomobile() + " automobile");
		System.out.println(car1);
		System.out.println(car3);
			
	}

}
